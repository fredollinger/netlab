#include <stdio.h>
#include <unistd.h>

int main(){
    pid_t pid;
    pid = fork();
    printf("pid %i\n", pid);
    while(1){
        if ( 0 == pid )
            printf("child\n");
        else
           printf("adult\n");
        sleep(1);
    }
}
