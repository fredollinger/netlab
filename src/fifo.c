#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>

#include "netlab.h"

// fifo.c will make a fifo in a forked process

void child() {
    char buf[1000];
    const char *resp="back at ya";
    int writefd, readfd;
    printf("child opening fifo: [%s] \n", FRAMP3_COMMAND_SOCK);
    readfd=open(FRAMP3_COMMAND_SOCK, O_RDONLY);
    writefd=open(FRAMP3_RESPONSE_SOCK, O_WRONLY, 0);
    printf("child SUCCESS\n");
    read(readfd, &buf, 999);
    printf("child read: [%s] \n", buf);
    sleep(2);
    write(writefd, resp,  strlen(resp));
}

void parent() {
    char buf[1000];
    const char *msg="Hello there\0";
    int writefd, readfd;
    int res = mkfifo(FRAMP3_COMMAND_SOCK, FILE_MODE);
    pid_t pid;
    res = mkfifo(FRAMP3_RESPONSE_SOCK, FILE_MODE);
    printf("making fifo: [%s] result: [%i] \n", FRAMP3_COMMAND_SOCK, res);

    pid = fork();

    if ( 0 == pid ) {
        child();
	exit(0);
    }

    writefd=open(FRAMP3_COMMAND_SOCK, O_WRONLY, 0);
    readfd=open(FRAMP3_RESPONSE_SOCK, O_RDONLY, 0);
    sleep(2);
    write(writefd, msg,  strlen(msg));
    printf("parent: writing: [%s] \n", msg);
    printf("parent: SUCCESS\n");
    read(readfd, &buf, 999);
    printf("parent read: [%s] \n", buf);
}

int main(int argc, char *argv[]){
    parent();
}
