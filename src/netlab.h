#include <sys/stat.h>

// These go TO our player
#define FRAMP3_COMMAND_SOCK "framp3-cmd.sock"

// These are responses FROM our player
#define FRAMP3_RESPONSE_SOCK "framp3-rsp.sock"
#define FILE_MODE ( S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH )

#define BARKER_SOCK "barker.sock"
