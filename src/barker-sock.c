#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "netlab.h"

int main(int argc, char *argv[]){
    const char *buf="Hello there\0";
    int writefd;
    int res = mkfifo(BARKER_SOCK, FILE_MODE);
    printf("making fifo: [%s] result: [%i] \n", BARKER_SOCK, res);
    writefd=open(BARKER_SOCK, O_WRONLY, 0);
    write(writefd, buf,  strlen(buf));
    printf("%s SUCCESS \n", argv[0]);
}
