#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "netlab.h"

int main(int argc, char *argv[]){
    char buf[1000];
    int readfd;
    printf("opening fifo: [%s] \n", BARKER_SOCK);
    readfd=open(BARKER_SOCK, O_RDONLY);
    printf("%s SUCCESS \n", argv[0]);
    while (1) {
        while(read(readfd, &buf, 999)){
            printf("reading...\n");
        }
        printf("read: [%s] \n", buf);
    }
}
